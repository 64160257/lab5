package com.paramet.week5;

public class SelectionSortApp {

    public static int findMinIndex(int[] arr, int pos) {
        int minIndex = pos;
        for(int i =pos+1; i<arr.length; i++) {
            if(arr[minIndex]>arr[i]) {
                minIndex = i;
            }
        }
        return minIndex;
    }

    public static void swap(int[] arr, int first, int second) {
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;
    }

    public static void selectionSort(int[] arr) {
        
        int minIndex = 0;
        for(int pos=0; pos<arr.length-1; pos++) {
            minIndex = findMinIndex(arr, pos);
            swap(arr, minIndex, pos);
        }
    }
}
